from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from .forms import SignupForm, LoginForm

def signup_view(request):
    form = SignupForm(request.POST)
    if request.method == 'POST' and form.is_valid():
        user = form.save()
        username_login = request.POST['username']
        login(request, user )
        request.session['username'] = username_login
        return redirect('landing')
    return render(request, 'accounts/signup.html', {'form':form})

def login_view(request):
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            username_login = request.POST['username']
            password_login = request.POST['password']
            user = authenticate(request, username=username_login, password=password_login)
            login(request, user)
            request.session['username'] = username_login
            return redirect('landing')
    else:
        form = LoginForm()
    return render(request, 'accounts/login.html', {'form': form})

def logout_view(request):
    if request :
        logout(request)
    return redirect('landing')