import os
from django.test import TestCase, Client
from django.urls import resolve
from .views import signup_view, login_view, logout_view

class Story9AccountsUnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/accounts/signup/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/accounts/logout/')
        self.assertEqual(response.status_code, 302)
    def test_method_view(self):
        found = resolve('/accounts/login/')
        self.assertEqual(found.func, login_view)
        found = resolve('/accounts/signup/')
        self.assertEqual(found.func, signup_view)
        found = resolve('/accounts/logout/')
        self.assertEqual(found.func, logout_view)

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from story9.settings import BASE_DIR
from time import sleep


class VisitorTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,'chromedriver'), chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
    
    def test_signup_logout_login(self):
        driver = self.browser
        driver.get('http://localhost:8000/')
        signup_button = driver.find_element_by_id("signup_button")
        signup_button.click()
        username_textbox = driver.find_element_by_id("id_username")
        username_textbox.send_keys("prabowi")
        password1_textbox = driver.find_element_by_id("id_password1")
        password1_textbox.send_keys("hyahyahya")
        password2_textbox = driver.find_element_by_id("id_password2")
        password2_textbox.send_keys("hyahyahya")
        button = driver.find_element_by_id("but")
        button.click()
        sleep(3)
        self.assertIn("prabowi", driver.page_source)
        logout_button = driver.find_element_by_id("logout_button")
        logout_button.click()
        sleep(3)
        self.assertIn("no name", driver.page_source)
        login_button = driver.find_element_by_id("login_button")
        login_button.click()
        username_textbox = driver.find_element_by_id("id_username")
        username_textbox.send_keys("prabowi")
        password1_textbox = driver.find_element_by_id("id_password")
        password1_textbox.send_keys("hyahyahya")
        button = driver.find_element_by_id("but")
        button.click()
        sleep(3)
        self.assertIn("prabowi", driver.page_source)