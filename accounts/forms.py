from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django import forms
from django.contrib.auth.models import User

class SignupForm(UserCreationForm):
    username = forms.CharField(label=("username"), widget=forms.PasswordInput(attrs={
        'class' : "form-control",
        'type' : "text",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))

    password1 = forms.CharField(label=("Password"), strip=False, widget=forms.PasswordInput(attrs={
        'class' : "form-control",
        'type' : "password",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        })
    )

    password2 = forms.CharField(label=("Password"), strip=False, widget=forms.PasswordInput(attrs={
        'class' : "form-control",
        'type' : "password",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        })
    )
    class Meta:
        model = User
        fields = ("username", "password1", "password2", )

class LoginForm(AuthenticationForm):
    username = forms.CharField(label=("username"), widget=forms.PasswordInput(attrs={
        'class' : "form-control",
        'type' : "text",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))

    password = forms.CharField(label=("Password"), strip=False, widget=forms.PasswordInput(attrs={
        'class' : "form-control",
        'type' : "password",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        })
    )
    class Meta:
        model = User
        fields = ("username", "password")