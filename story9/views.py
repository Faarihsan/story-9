from django.shortcuts import render
from django.contrib.auth import authenticate

def landing(request):
    if request.user.is_authenticated :
        user = request.session.get('username')
    else :
        user = 'no name'
    return render(request, 'landing.html', {'user':user})